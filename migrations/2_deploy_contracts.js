const {parseEther} = require("ethers/lib/utils");
const MerkleDistributor = artifacts.require('MerkleDistributor')

const merkleRoot =
    "0xa3b403d282f94ed81db82e426bc33987edf20ae0f0666faef97b653e0f4e5025";
const vaultPercentage = "10000000000000000000000000";
const vaultTime = "100";
const registry = "0xc3E589056Ece16BCB88c6f9318e9a7343b663522";
const value = parseEther("2");

module.exports = async function (deployer) {
    const forwarder = require('../build/gsn/Forwarder').address
    await deployer.deploy(MerkleDistributor, merkleRoot,
        vaultPercentage,
        vaultTime,
        registry,
        forwarder,
        {
            value,
        })

    console.log(`Deployed MerkleDistributor at ${MerkleDistributor.address} with forwarder ${forwarder}`)
}
