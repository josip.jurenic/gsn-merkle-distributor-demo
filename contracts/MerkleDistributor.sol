// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.9;

import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@opengsn/contracts/src/ERC2771Recipient.sol";
import "@q-dev/contracts/tokeneconomics/QVault.sol";
import "@q-dev/contracts/ContractRegistry.sol";
import "./interfaces/IMerkleDistributor.sol";

contract MerkleDistributor is IMerkleDistributor, ERC2771Recipient {
    bytes32 public immutable override merkleRoot;
    uint256 public immutable override vaultPercentage;
    uint256 public immutable override vaultTime;

    ContractRegistry private registry;

    // This is a packed array of booleans.
    mapping(uint256 => uint256) private claimedBitMap;

    constructor(
        bytes32 merkleRoot_,
        uint256 vaultPercentage_,
        uint256 vaultTime_,
        address registry_,
        address forwarder_
    ) payable {
        merkleRoot = merkleRoot_;
        vaultPercentage = vaultPercentage_;
        vaultTime = vaultTime_;
        registry = ContractRegistry(registry_);
        _setTrustedForwarder(forwarder_);
    }

    function isClaimed(uint256 index) public view override returns (bool) {
        uint256 claimedWordIndex = index / 256;
        uint256 claimedBitIndex = index % 256;
        uint256 claimedWord = claimedBitMap[claimedWordIndex];
        uint256 mask = (1 << claimedBitIndex);
        return claimedWord & mask == mask;
    }

    function _setClaimed(uint256 index) private {
        uint256 claimedWordIndex = index / 256;
        uint256 claimedBitIndex = index % 256;
        claimedBitMap[claimedWordIndex] =
        claimedBitMap[claimedWordIndex] |
        (1 << claimedBitIndex);
    }

    function claim(
        uint256 index,
        address account,
        uint256 amount,
        bytes32[] calldata merkleProof
    ) external override {
        require(!isClaimed(index), "MerkleDistributor: Drop already claimed.");

        // Verify the merkle proof.
        bytes32 node = keccak256(abi.encodePacked(index, account, amount));
        require(
            MerkleProof.verify(merkleProof, merkleRoot, node),
            "MerkleDistributor: Invalid proof."
        );

        // Mark it claimed.
        _setClaimed(index);

        // Lock percentage of amount.
        uint256 _vaultAmount = (amount * vaultPercentage) / getDecimal();

        if (_vaultAmount >= 10 ether) {
            QVault _vault = QVault(registry.mustGetAddress("tokeneconomics.qVault"));
            _vault.depositOnBehalfOf{ value: _vaultAmount }(
                account,
                block.timestamp,
                block.timestamp + vaultTime
            );
        } else {
            _vaultAmount = 0;
        }

        // Send rest of Q.
        (bool sent, ) = account.call{ value: amount - _vaultAmount }("");
        require(sent, "MerkleDistributor: Transfer failed.");

        emit Claimed(index, account, amount);
    }
}
