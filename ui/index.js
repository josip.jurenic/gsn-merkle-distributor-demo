const ethers = require('ethers')
const {RelayProvider} = require('@opengsn/provider')

const paymasterAddress = require('../build/gsn/Paymaster').address
const contractArtifact = require('../build/contracts/MerkleDistributor.json')
const contractAbi = contractArtifact.abi

let theContract
let provider
let gsnProvider

async function initContract() {

    if (!window.ethereum) {
        throw new Error('provider not found')
    }
    window.ethereum.on('accountsChanged', () => {
        console.log('acct');
        window.location.reload()
    })
    window.ethereum.on('chainChanged', () => {
        console.log('chainChained');
        window.location.reload()
    })
    const networkId = await window.ethereum.request({method: 'net_version'})

    gsnProvider = await RelayProvider.newProvider({
        provider: window.ethereum,
        config: {
            loggerConfiguration: {logLevel: 'debug'},
            paymasterAddress
        }
    }).init()

    provider = new ethers.providers.Web3Provider(gsnProvider)

    const network = await provider.getNetwork()
    const artifactNetwork = contractArtifact.networks[networkId]
    if (!artifactNetwork)
        throw new Error('Can\'t find deployment on network ' + networkId)
    const contractAddress = artifactNetwork.address
    theContract = new ethers.Contract(
        contractAddress, contractAbi, provider.getSigner())

    await listenToEvents()
    return {contractAddress, network}
}

async function contractCall() {
    await window.ethereum.send('eth_requestAccounts')

    const merkle_address = document.getElementById('merkle_address').value
    const response = await fetch(`http://15.161.55.248:3000/claimers/${merkle_address}`)
    const claimersResponse = await response.json()

    console.log(claimersResponse)

    const txOptions = {gasPrice: await provider.getGasPrice()}
    console.log(theContract)
    const transaction = await theContract.claim(claimersResponse?.index,
        claimersResponse?.address,
        claimersResponse?.amount,
        claimersResponse?.proof, txOptions)
    const hash = transaction.hash
    console.log(`Transaction ${hash} sent`)
    const receipt = await transaction.wait()
    console.log(`Mined in block: ${receipt.blockNumber}`)
}

let logview

function log(message) {
    message = message.replace(/(0x\w\w\w\w)\w*(\w\w\w\w)\b/g, '<b>$1...$2</b>')
    if (!logview) {
        logview = document.getElementById('logview')
    }
    logview.innerHTML = message + "<br>\n" + logview.innerHTML
}

async function listenToEvents() {

    theContract.on('Claimed', (index, account, amount, rawEvent) => {
        log(`index, account, amount: ${index}, ${account}, ${amount}`)
        console.log(`index, account, amount: ${index}, ${account}, ${amount}`)
    })
}

window.app = {
    initContract,
    contractCall,
    log
}

